import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import { MatDialog } from '@angular/material';
import { ErrorDialogComponent } from '../_components';
import { LoginResponse } from '../_interfaces';
import { authKey } from '../keys';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  /**
   * Form submission check for spinner.
   */
  public submitted = false;

  /**
   * Form validation.
   */
  public form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email,
      Validators.maxLength(255)
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(255)
    ])
  });

  /**
   * LoginComponent constructor.
   *
   * @param {HttpClient} http
   * @param {Router} router
   * @param {MatDialog} dialog
   */
  constructor(
    private http: HttpClient,
    private router: Router,
    private dialog: MatDialog
  ) {}

  /**
   * Retrieve form validation email value.
   */
  public get email(): AbstractControl {
    return this.form.get('email');
  }

  /**
   * Retrieve form validation password value.
   */
  public get password(): AbstractControl {
    return this.form.get('password');
  }

  /**
   * Handle login attempt.
   */
  public onSubmit(): void {
    this.submitted = true;
    this.form.value.strategy = 'local';

    this.http
      .post<LoginResponse>(`/authentication`, this.form.value)
      .subscribe(
        (res: LoginResponse) => {
          localStorage.setItem(authKey, res.accessToken);

          this.router.navigateByUrl('/posts');
        },
        (err: HttpErrorResponse) => {
          const message = (err.status === 401)
            ? 'Your email address or password is incorrect.'
            : 'We are having trouble logging you in. Please try again later.';

          this.dialog
            .open(ErrorDialogComponent, { width: '250px', data: { message }})
            .beforeClose()
            .subscribe(() => this.submitted = false);
        }
      );
  }

}
