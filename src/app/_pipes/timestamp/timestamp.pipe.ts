import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'timestamp'
})
export class TimestampPipe implements PipeTransform {

  /**
   * Convert an ISO 8601 timestamp value to a neat, user friendly time with moment.
   * YYYY-MM-DD HH:mm:ss.SSS Z
   */
  public transform(time: string): string {
    const format = 'YYYY-MM-DD HH:mm:ss.SSS Z';

    if (moment(time, [format], true).isValid()) {
      return moment(time, format).fromNow();
    }

    return '';
  }

}
