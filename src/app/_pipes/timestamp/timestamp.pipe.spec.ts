import { TimestampPipe } from './timestamp.pipe';
import * as moment from 'moment';

describe('TimestampPipe', () => {
  const pipe = new TimestampPipe();

  it('convert timestamp to date time', () => {
    const timestamp = moment().subtract(2, 'days').format('YYYY-MM-DD HH:mm:ss.SSS Z');

    expect(pipe.transform(timestamp)).toEqual('2 days ago');
  });

  it('return empty string on invalid date', () => {
    expect(pipe.transform('incorrect timestamp')).toEqual('');
  });
});
