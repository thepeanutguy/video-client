import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-error-dialog',
  template: `
    <h1 mat-dialog-title>Whoops!</h1>
    <div mat-dialog-content>
      <p>{{ data.message }}</p>
    </div>
    <div mat-dialog-actions>
      <button color="primary" [mat-dialog-close]="true" mat-raised-button cdkFocusInitial>Okay</button>
    </div>
  `,
  styles: [`
    p {
      margin: 1em 0 2em 0;
    }

    button {
      margin-left: auto;
      margin-right: -0.75em;
    }
  `]
})
export class ErrorDialogComponent {

  /**
   * ErrorDialogComponent constructor.
   *
   * @param {MatDialogRef} dialogRef
   * @param {any} data
   */
  constructor(
    private dialogRef: MatDialogRef<ErrorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

}
