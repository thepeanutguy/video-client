import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { authKey } from '../../keys';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  /**
   * Header title, defaulting to app name.
   */
  @Input('title') title = 'Vid';

  /**
   * HeaderComponent constructor.
   *
   * @param {Router} router
   */
  constructor(
    private router: Router
  ) { }

  /**
   * ...
   */
  public ngOnInit() {
    this.title = this.title || 'Vid';

    if (this.title !== 'Vid') {
      this.title = `Vid | ${this.title.trim()}`;
    }
  }

  /**
   * Navigate to the users page.
   */
  public account(): void {
    this.router.navigateByUrl('/account');
  }


  /**
   * Clear the authentication token from storage and navigate to the logout page.
   */
  public logout(): void {
    localStorage.removeItem(authKey);

    this.router.navigateByUrl('/login');
  }

}
