import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { authKey } from '../../keys';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  /**
   * Add an authorization token, if possible.
   *
   * @param {HttpRequest<any>} req
   * @param {HttpHandler} next
   */
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string|null = localStorage.getItem(authKey);

    if (req.url.slice(21, 36) !== '/authentication' && token !== null) {
      return next.handle(
        req.clone({ headers: req.headers.set('Authorization', token) })
      );
    }

    return next.handle(req);
  }

}
