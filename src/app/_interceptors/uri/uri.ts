import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import url from '../../../config/url';

@Injectable()
export class UriInjectionInterceptor implements HttpInterceptor {

  /**
   * Prepend all outgoing HTTP requests with the URI of the API server.
   *
   * @param {HttpRequest<any>} req
   * @param {HttpHandler} next
   */
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({ url: url + req.url });

    return next.handle(req);
  }

}
