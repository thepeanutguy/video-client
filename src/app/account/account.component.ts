import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../_interfaces';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  /**
   * ...
   */
  public user: User;

  /**
   * AccountComponent constructor.
   *
   * @param {HttpClient} http
   */
  constructor(
    private http: HttpClient
  ) { }

  /**
   * ...
   */
  public ngOnInit(): void {
    this.http
      .get<User>(`/users`)
      .subscribe(
        (res) => console.log(res),
        (err: HttpErrorResponse) => console.error(err)
      );
  }

}
