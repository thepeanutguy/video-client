import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import { Post, PostResponse } from '../_interfaces';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  /**
   * Video feed.
   */
  public posts: Post[] = [];

  /**
   * PostsComponent constructor.
   *
   * @param {HttpClient} http
   */
  constructor(
    private http: HttpClient
  ) { }

  /**
   * Retrieve ...
   */
  public ngOnInit(): void {
    this.http
      .get<PostResponse>(`/posts`)
      .subscribe(
        (res: PostResponse) => {
          console.log('Received data:', res);

          this.posts = res.data;
        },
        (err: HttpErrorResponse) => console.error(err)
      );
  }

}
