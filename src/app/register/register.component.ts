import { Component } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ErrorDialogComponent } from '../_components';
import { User } from '../_interfaces';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  /**
   * Form submission check for spinner.
   */
  public submitted = false;

  /**
   * Form validation.
   */
  public form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email,
      Validators.maxLength(255)
    ]),
    firstName: new FormControl('', [
      Validators.required,
      Validators.maxLength(255)
    ]),
    lastName: new FormControl('', [
      Validators.required,
      Validators.maxLength(255)
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(255)
    ])
  });

  constructor(
    private http: HttpClient,
    private router: Router,
    private dialog: MatDialog
  ) { }

  /**
   * Retrieve form validation email value.
   */
  public get email(): AbstractControl {
    return this.form.get('email');
  }

  /**
   * ...
   */
  public get firstName(): AbstractControl {
    return this.form.get('firstName');
  }

  /**
   * ...
   */
  public get lastName(): AbstractControl {
    return this.form.get('lastName');
  }

  /**
   * ...
   */
  public get password(): AbstractControl {
    return this.form.get('password');
  }

  /**
   * Handle registration attempt.
   */
  public onSubmit(): void {
    this.submitted = true;

    this.http
      .post<User>('/users', this.form.value)
      .subscribe(
        (res: User) => {
          console.log(res);

          this.submitted = true;
        },
        (err: HttpErrorResponse) => {
          this.dialog
            .open(ErrorDialogComponent, { width: '250px', data: {
              message: 'We are having trouble logging you in. Please try again later.'
            }})
            .beforeClose()
            .subscribe(() => this.submitted = false);
        }
      );
  }

}
