export interface Post {
  message: string;
  url: string;
  createdAt: string;
  updateAt: string;
}

export interface PostResponse {
  title: number;
  limit: number;
  skip: number;
  data: Post[];
}
