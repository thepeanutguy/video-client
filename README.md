# Video Client

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.2.  
The API is located [here](https://gitlab.com/thepeanutguy/video).

## System Requirements 

- node.js 9.3+
- yarn

## Development and Running Locally 

- Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
- Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Production Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Testing

- Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
- Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Changelog

__0.1.0__

- Initial release

## License

Copyright (c) 2016

Licensed under the [GNU GPLv3 LICENCE](LICENSE).
